// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_checks_core::impl_prelude::*;
use crates::itertools::Itertools;

/// The CR/LF line ending.
const CR_LF_ENDING: &str = "\r\n";
/// A symbol to replace `\r` characters so that they appear in the error message.
const CARRIAGE_RETURN_SYMBOL: &str = "\u{23ce}";

/// Checks for bad whitespace using Git's built-in checks.
///
/// This is attribute-driven, so any `gitattributes(5)` files may be used to suppress spirious
/// errors from this check.
#[derive(Builder, Debug, Default, Clone, Copy)]
#[builder(field(private))]
pub struct CheckWhitespace {}

impl CheckWhitespace {
    /// Create a new builder.
    pub fn builder() -> CheckWhitespaceBuilder {
        CheckWhitespaceBuilder::default()
    }

    fn add_error(result: &mut CheckResult, output: &[u8], content: &dyn Content) {
        // Check for CR/LF line endings. This is done because most editors will mask their
        // existence making the "trailing whitespace" hard to find.
        let output = String::from_utf8_lossy(output);
        let crlf_msg = if output.contains(CR_LF_ENDING) {
            " including CR/LF line endings"
        } else {
            ""
        };
        let formatted_output = output
            .split('\n')
            // Git seems to add a trailing newline to its output, so drop the last line.
            .dropping_back(1)
            .map(|line| format!("        {}\n", line))
            .join("")
            .replace('\r', CARRIAGE_RETURN_SYMBOL);

        result.add_error(format!(
            "{}adds bad whitespace{}:\n\n{}",
            commit_prefix(content),
            crlf_msg,
            formatted_output,
        ));
    }
}

impl Check for CheckWhitespace {
    fn name(&self) -> &str {
        "check-whitespace"
    }

    fn check(&self, ctx: &CheckGitContext, commit: &Commit) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        let diff_tree = ctx
            .git()
            .arg("diff-tree")
            .arg("--no-commit-id")
            .arg("--root")
            .arg("-c")
            .arg("--check")
            .arg(commit.sha1.as_str())
            .output()
            .map_err(|err| GitError::subcommand("diff-tree", err))?;
        if !diff_tree.status.success() {
            Self::add_error(&mut result, &diff_tree.stdout, commit);
        }

        Ok(result)
    }
}

impl TopicCheck for CheckWhitespace {
    fn name(&self) -> &str {
        "check-whitespace"
    }

    fn check(&self, ctx: &CheckGitContext, topic: &Topic) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        let diff_tree = ctx
            .git()
            .arg("diff-tree")
            .arg("--no-commit-id")
            .arg("--root")
            .arg("-c")
            .arg("--check")
            .arg(topic.base.as_str())
            .arg(topic.sha1.as_str())
            .output()
            .map_err(|err| GitError::subcommand("diff-tree", err))?;
        if !diff_tree.status.success() {
            Self::add_error(&mut result, &diff_tree.stdout, topic);
        }

        Ok(result)
    }
}

#[cfg(feature = "config")]
pub(crate) mod config {
    use crates::git_checks_config::{CommitCheckConfig, IntoCheck, TopicCheckConfig};
    use crates::inventory;
    #[cfg(test)]
    use crates::serde_json;

    use CheckWhitespace;

    /// Configuration for the `CheckWhitespace` check.
    ///
    /// No configuration available.
    ///
    /// This check is registered as a commit check with the name `"check_whitespace"` and a topic
    /// check with the name `"check_whitespace/topic"`.
    #[derive(Deserialize, Debug)]
    pub struct CheckWhitespaceConfig {}

    impl IntoCheck for CheckWhitespaceConfig {
        type Check = CheckWhitespace;

        fn into_check(self) -> Self::Check {
            CheckWhitespace::default()
        }
    }

    register_checks! {
        CheckWhitespaceConfig {
            "check_whitespace" => CommitCheckConfig,
            "check_whitespace/topic" => TopicCheckConfig,
        },
    }

    #[test]
    fn test_check_whitespace_config_empty() {
        let json = json!({});
        serde_json::from_value::<CheckWhitespaceConfig>(json).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use test::*;
    use CheckWhitespace;

    const DEFAULT_TOPIC: &str = "829cdf8cb069b8f8a634a034d3f85089271601cf";
    const ALL_IGNORED_TOPIC: &str = "3a87e0f3f7430bbb81ebbd8ae8764b7f26384f1c";
    const ALL_IGNORED_BLANKET_TOPIC: &str = "92cac7579a26f7d8449512476bd64b3000688fd5";

    #[test]
    fn test_check_whitespace_builder_default() {
        assert!(CheckWhitespace::builder().build().is_ok());
    }

    #[test]
    fn test_check_whitespace_defaults() {
        let check = CheckWhitespace::default();
        let result = run_check("test_check_whitespace_defaults", DEFAULT_TOPIC, check);
        test_result_errors(
            result,
            &[
                "commit 829cdf8cb069b8f8a634a034d3f85089271601cf adds bad whitespace including \
                 CR/LF line endings:\n\
                 \n        \
                 crlf-file:1: trailing whitespace.\n        \
                 +This file contains CRLF lines.\u{23ce}\n        \
                 crlf-file:2: trailing whitespace.\n        \
                 +\u{23ce}\n        \
                 crlf-file:3: trailing whitespace.\n        \
                 +line1\u{23ce}\n        \
                 crlf-file:4: trailing whitespace.\n        \
                 +line2\u{23ce}\n        \
                 crlf-mixed-file:3: trailing whitespace.\n        \
                 +crlf\u{23ce}\n        \
                 extra-newlines:2: new blank line at EOF.\n        \
                 mixed-tabs-spaces:3: space before tab in indent.\n        \
                 +   \tmixed indent\n        \
                 trailing-spaces:3: trailing whitespace.\n        \
                 +trailing \n        \
                 trailing-tab:3: trailing whitespace.\n        \
                 +trailing\t\n",
            ],
        );
    }

    #[test]
    fn test_check_whitespace_all_ignored() {
        let check = CheckWhitespace::default();
        run_check_ok(
            "test_check_whitespace_all_ignored",
            ALL_IGNORED_TOPIC,
            check,
        );
    }

    #[test]
    fn test_check_whitespace_all_ignored_blanket() {
        let check = CheckWhitespace::default();
        run_check_ok(
            "test_check_whitespace_all_ignored_blanket",
            ALL_IGNORED_BLANKET_TOPIC,
            check,
        );
    }
}
