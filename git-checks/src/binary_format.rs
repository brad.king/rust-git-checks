// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

const AR_MAGIC: &[u8] = b"!<arch>\n";
const ELF_MAGIC: &[u8] = &[0x7f, 0x45, 0x4c, 0x46]; // 0x7f ELF
const MACHO_MAGIC: &[u8] = &[0xcf, 0xfa, 0xed, 0xfe];
const MACHO_CIGAM: &[u8] = &[0xfe, 0xed, 0xfa, 0xcf];
const MACHO_FAT_MAGIC: &[u8] = &[0xca, 0xfe, 0xba, 0xbe];
const MACHO_FAT_CIGAM: &[u8] = &[0xbe, 0xba, 0xfe, 0xca];

pub(crate) enum BinaryFormat {
    AR,
    ELF,
    MachO,
}

impl BinaryFormat {
    pub(crate) fn name(&self) -> &'static str {
        match *self {
            BinaryFormat::AR => "AR",
            BinaryFormat::ELF => "ELF",
            BinaryFormat::MachO => "Mach-O",
        }
    }

    pub(crate) fn is_executable(&self) -> bool {
        match *self {
            BinaryFormat::AR => false,
            _ => true,
        }
    }
}

pub(crate) fn detect_binary_format<C>(content: C) -> Option<BinaryFormat>
where
    C: AsRef<[u8]>,
{
    let content = content.as_ref();
    if content.starts_with(ELF_MAGIC) {
        Some(BinaryFormat::ELF)
    } else if content.starts_with(AR_MAGIC) {
        Some(BinaryFormat::AR)
    } else if content.starts_with(MACHO_MAGIC)
        || content.starts_with(MACHO_CIGAM)
        || content.starts_with(MACHO_FAT_MAGIC)
        || content.starts_with(MACHO_FAT_CIGAM)
    {
        Some(BinaryFormat::MachO)
    } else {
        None
    }
}
