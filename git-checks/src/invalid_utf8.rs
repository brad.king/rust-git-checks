// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_checks_core::impl_prelude::*;

use std::char::REPLACEMENT_CHARACTER;

/// A check which denies commits which modify files containing special characters.
///
/// Files may be marked as binary by unsetting the `text` attribute.
#[derive(Builder, Debug, Default, Clone, Copy)]
#[builder(field(private))]
pub struct InvalidUtf8 {}

impl InvalidUtf8 {
    /// Create a new builder.
    pub fn builder() -> InvalidUtf8Builder {
        InvalidUtf8Builder::default()
    }
}

impl ContentCheck for InvalidUtf8 {
    fn name(&self) -> &str {
        "invalid-utf8"
    }

    fn check(
        &self,
        ctx: &CheckGitContext,
        content: &dyn Content,
    ) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        for diff in content.diffs() {
            match diff.status {
                StatusChange::Added | StatusChange::Modified(_) => (),
                _ => continue,
            }

            let diff_attr = ctx.check_attr("diff", diff.name.as_path())?;
            if let AttributeState::Unset = diff_attr {
                // Binary files should not be handled here.
                continue;
            }

            // FIXME: does this work for binary files?
            let patch = match content.path_diff(&diff.name) {
                Ok(s) => s,
                Err(err) => {
                    result.add_alert(
                        format!(
                            "{}failed to get the diff for file `{}`: {}.",
                            commit_prefix(content),
                            diff.name,
                            err,
                        ),
                        true,
                    );
                    continue;
                },
            };

            for line in patch.lines().filter(|line| line.starts_with('+')) {
                if line.contains(REPLACEMENT_CHARACTER) {
                    // Escape instances of backticks and backslashes.
                    let safe_line = line[1..].replace('\\', "\\\\").replace('`', "\\`");
                    result.add_error(format!(
                        "{}invalid utf-8 sequence added in \
                         `{}`: `{}`.",
                        commit_prefix_str(content, "not allowed;"),
                        diff.name,
                        safe_line,
                    ));
                }
            }
        }

        Ok(result)
    }
}

#[cfg(feature = "config")]
pub(crate) mod config {
    use crates::git_checks_config::{CommitCheckConfig, IntoCheck, TopicCheckConfig};
    use crates::inventory;
    #[cfg(test)]
    use crates::serde_json;

    use InvalidUtf8;

    /// Configuration for the `InvalidUtf8` check.
    ///
    /// No configuration available.
    ///
    /// This check is registered as a commit check with the name `"invalid_utf8"` and a topic check
    /// with the name `"invalid_utf8/topic"`.
    #[derive(Deserialize, Debug)]
    pub struct InvalidUtf8Config {}

    impl IntoCheck for InvalidUtf8Config {
        type Check = InvalidUtf8;

        fn into_check(self) -> Self::Check {
            InvalidUtf8::default()
        }
    }

    register_checks! {
        InvalidUtf8Config {
            "invalid_utf8" => CommitCheckConfig,
            "invalid_utf8/topic" => TopicCheckConfig,
        },
    }

    #[test]
    fn test_invalid_utf8_config_empty() {
        let json = json!({});
        serde_json::from_value::<InvalidUtf8Config>(json).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use test::*;
    use InvalidUtf8;

    const BAD_TOPIC: &str = "cf16b71a21023320ffab7b3f7673dc62f33e5022";
    const FIX_TOPIC: &str = "e8763477e9ebef4a61d130724cee9e29b13f857e";

    #[test]
    fn test_invalid_utf8_builder_default() {
        assert!(InvalidUtf8::builder().build().is_ok());
    }

    #[test]
    fn test_invalid_utf8() {
        let check = InvalidUtf8::default();
        let result = run_check("test_invalid_utf8", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit cf16b71a21023320ffab7b3f7673dc62f33e5022 not allowed; invalid utf-8 sequence \
             added in `invalid-utf8`: `This file contains an invalid utf-8 sequence: \
             \u{fffd}`.",
        ]);
    }

    // TODO: Add tests for -diff files.

    #[test]
    fn test_invalid_utf8_topic() {
        let check = InvalidUtf8::default();
        let result = run_topic_check("test_invalid_utf8_topic", BAD_TOPIC, check);
        test_result_errors(result, &[
            "invalid utf-8 sequence added in `invalid-utf8`: `This file contains an invalid utf-8 \
             sequence: \u{fffd}`.",
        ]);
    }

    #[test]
    fn test_invalid_utf8_topic_fixed() {
        let check = InvalidUtf8::default();
        run_topic_check_ok("test_invalid_utf8_topic_fixed", FIX_TOPIC, check);
    }
}
