// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_checks_core::impl_prelude::*;

/// Check for files which lack an end-of-line at the end of the file.
#[derive(Builder, Debug, Default, Clone, Copy)]
pub struct CheckEndOfLine {}

impl CheckEndOfLine {
    /// Create a new builder.
    pub fn builder() -> CheckEndOfLineBuilder {
        CheckEndOfLineBuilder::default()
    }
}

impl ContentCheck for CheckEndOfLine {
    fn name(&self) -> &str {
        "check-end-of-line"
    }

    fn check(
        &self,
        _: &CheckGitContext,
        content: &dyn Content,
    ) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        for diff in content.diffs() {
            match diff.status {
                StatusChange::Added | StatusChange::Modified(_) => (),
                _ => continue,
            }

            // Ignore symlinks; they only end with newlines if they point to a file with a newline
            // at the end of its name.
            if diff.new_mode == "120000" {
                continue;
            }

            // FIXME: does this work for binary files?
            let patch = match content.path_diff(&diff.name) {
                Ok(s) => s,
                Err(err) => {
                    result.add_alert(
                        format!(
                            "{}failed to get the diff for file `{}`: {}",
                            commit_prefix(content),
                            diff.name,
                            err,
                        ),
                        true,
                    );
                    continue;
                },
            };

            let has_missing_newline = patch
                .lines()
                .last()
                .map_or(false, |line| line == "\\ No newline at end of file");

            if has_missing_newline {
                result.add_error(format!(
                    "{}missing newline at the end of file in `{}`.",
                    commit_prefix_str(content, "is not allowed;"),
                    diff.name,
                ));
            }
        }

        Ok(result)
    }
}

#[cfg(feature = "config")]
pub(crate) mod config {
    use crates::git_checks_config::{CommitCheckConfig, IntoCheck, TopicCheckConfig};
    use crates::inventory;
    #[cfg(test)]
    use crates::serde_json;

    use CheckEndOfLine;

    /// Configuration for the `CheckEndOfLine` check.
    ///
    /// No configuration available.
    ///
    /// This check is registered as a commit check with the name `"check_end_of_line"` and a topic
    /// check with the name `"check_end_of_line/topic"`.
    #[derive(Deserialize, Debug)]
    pub struct CheckEndOfLineConfig {}

    impl IntoCheck for CheckEndOfLineConfig {
        type Check = CheckEndOfLine;

        fn into_check(self) -> Self::Check {
            CheckEndOfLine::default()
        }
    }

    register_checks! {
        CheckEndOfLineConfig {
            "check_end_of_line" => CommitCheckConfig,
            "check_end_of_line/topic" => TopicCheckConfig,
        },
    }

    #[test]
    fn test_check_end_of_line_config_empty() {
        let json = json!({});
        serde_json::from_value::<CheckEndOfLineConfig>(json).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use test::*;
    use CheckEndOfLine;

    const BAD_COMMIT: &str = "829cdf8cb069b8f8a634a034d3f85089271601cf";
    const FIX_COMMIT: &str = "767dd1c173175d85e0f7de23dcd286f5a83617b1";

    #[test]
    fn test_check_end_of_line_builder_default() {
        assert!(CheckEndOfLine::builder().build().is_ok());
    }

    #[test]
    fn test_check_end_of_line() {
        let check = CheckEndOfLine::default();
        let result = run_check("test_check_end_of_line", BAD_COMMIT, check);
        test_result_errors(result, &[
            "commit 829cdf8cb069b8f8a634a034d3f85089271601cf is not allowed; missing newline at \
             the end of file in `missing-newline-eof`.",
        ]);
    }

    #[test]
    fn test_check_end_of_line_topic() {
        let check = CheckEndOfLine::default();
        let result = run_topic_check("test_check_end_of_line_topic", BAD_COMMIT, check);
        test_result_errors(
            result,
            &["missing newline at the end of file in `missing-newline-eof`."],
        );
    }

    #[test]
    fn test_check_end_of_line_removal() {
        let check = CheckEndOfLine::default();
        let conf = make_check_conf(&check);

        let result = test_check_base(
            "test_check_end_of_line_removal",
            FIX_COMMIT,
            BAD_COMMIT,
            &conf,
        );
        test_result_ok(result);
    }

    #[test]
    fn test_check_end_of_line_topic_fixed() {
        let check = CheckEndOfLine::default();
        run_topic_check_ok("test_check_end_of_line_topic_fixed", FIX_COMMIT, check);
    }
}
