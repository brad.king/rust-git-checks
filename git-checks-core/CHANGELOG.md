# v1.0.0

## What's new

  * This crate has now been split out from `git-checks-3.9.1`. This allows code
    which just needs to run checks to depend on a smaller crate.
  * No more `error-chain`.
  * Core traits are now using `Box<dyn Trait>` instead of a forced error type.
  * No more panicking. Errors are now propogated up instead of assuming `git`
    is well-behaved over time.
