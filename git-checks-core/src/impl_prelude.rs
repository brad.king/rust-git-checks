// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Module for importing all types and traits for implementing checks.

pub use std::error::Error;

pub use crates::git_workarea::CommitId;
pub use crates::git_workarea::GitError;
pub use crates::git_workarea::Identity;

pub use check::BranchCheck;
pub use check::Check;
pub use check::CheckResult;
pub use check::ContentCheck;
pub use check::TopicCheck;
pub use commit::Commit;
pub use commit::CommitError;
pub use commit::Content;
pub use commit::DiffInfo;
pub use commit::FileName;
pub use commit::StatusChange;
pub use commit::Topic;
pub use context::AttributeState;
pub use context::CheckGitContext;
pub use utils::SubmoduleContext;

/// Create a prefix to a message referencing the associated.
pub fn commit_prefix(content: &dyn Content) -> String {
    if let Some(sha1) = content.sha1() {
        format!("commit {} ", sha1)
    } else {
        String::new()
    }
}

/// Create a prefix to a message referencing the associated with a description string.
pub fn commit_prefix_str<M>(content: &dyn Content, msg: M) -> String
where
    M: AsRef<str>,
{
    if let Some(sha1) = content.sha1() {
        format!("commit {} {} ", sha1, msg.as_ref())
    } else {
        String::new()
    }
}
